package br.ucsal.testequalidade20162.tui;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

import br.ucsal.testequalidade20162.business.CondutorBo;
import br.ucsal.testequalidade20162.domain.Multa;
import br.ucsal.testequalidade20162.domain.TipoMultaEnum;
import br.ucsal.testequalidade20162.exception.RegistroDuplicadoException;
import br.ucsal.testequalidade20162.exception.RegistroNaoEncontradoException;
import br.ucsal.testequalidade20162.util.DateTimeUtil;

public class CondutorTui {

	private static Scanner sc = new Scanner(System.in);

	public static void cadastrarCondutor() {
		Integer numeroCnh = obterNumeroCnh();
		String nome = obterNomeCondutor();
		cadastrarCondutor(numeroCnh, nome);
	}

	private static void cadastrarCondutor(Integer numeroCnh, String nome) {
		try {
			CondutorBo.inserir(numeroCnh, nome);
			System.out.println("Condutor cadastrado com sucesso.");
		} catch (RegistroDuplicadoException e) {
			System.out.println("Já existe um condutor cadastrado para esta CNH.");
		}
	}

	private static String obterNomeCondutor() {
		System.out.println("Informe o nome do condutor:");
		String nome = sc.nextLine();
		return nome;
	}

	private static Integer obterNumeroCnh() {
		System.out.println("Informe o número da CNH:");
		Integer numeroCnh = sc.nextInt();
		sc.nextLine();
		return numeroCnh;
	}

	public static void cadastrarMulta() {
		Integer numeroCnh = obterNumeroCnh();
		LocalDateTime dataHora = obterDataHora();
		String local = obterLocal();
		TipoMultaEnum tipoMulta = obterTipoMulta();

		cadastrarMulta(numeroCnh, dataHora, local, tipoMulta);
	}

	private static void cadastrarMulta(Integer numeroCnh, LocalDateTime dataHora, String local, TipoMultaEnum tipoMulta) {
		try {
			Multa multa = new Multa();
			multa.setDataHora(dataHora);
			multa.setLocal(local);
			multa.setTipo(tipoMulta);
			CondutorBo.registrarMulta(numeroCnh, multa);
			System.out.println("Multa cadastrada com sucesso.");
		} catch (RegistroNaoEncontradoException e) {
			System.out.println("Condutor não encontrado.");
		} catch (RegistroDuplicadoException e) {
			System.out.println("Já existe uma multa cadastrada para este condutor nesta data/hora.");
		}
	}

	private static TipoMultaEnum obterTipoMulta() {
		System.out.println("Tipo da multa (" + TipoMultaEnum.listarTiposMulta() + "):");
		String tipoMultaString = sc.nextLine();
		TipoMultaEnum tipoMulta = TipoMultaEnum.valueOf(tipoMultaString);
		return tipoMulta;
	}

	private static String obterLocal() {
		System.out.println("Local:");
		String local = sc.nextLine();
		return local;
	}

	private static LocalDateTime obterDataHora() {
		System.out.println("Data/hora:");
		LocalDateTime dataHora = null;
		do {
			try {
				String dataHoraString = sc.nextLine();
				dataHora = DateTimeUtil.parse(dataHoraString);
			} catch (DateTimeParseException e) {
				System.out.println("Data/hora inválida.");
			}
		} while (dataHora == null);
		return dataHora;
	}

	public static void main(String[] args) {
		LocalDateTime ld = obterDataHora();
		System.out.println(ld);
		System.out.println(DateTimeUtil.format(ld));
	}

}
